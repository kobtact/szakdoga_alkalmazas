CREATE DATABASE IF NOT EXISTS testdatabase DEFAULT CHARACTER SET utf8;
USE testdatabase;

CREATE OR REPLACE TABLE product (
	id			int(4)			NOT NULL	AUTO_INCREMENT	PRIMARY KEY,
	p_name		varchar(100)	NOT NULL,
	category	varchar(50)		NOT NULL,
	price		int(10)			NOT NULL,
	quantity	int(10)			NOT NULL	DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO product (p_name, category, price, quantity) VALUES ('GeForce GTX 1080 Ti', 'video card', 2000000, 30);
INSERT INTO product (p_name, category, price, quantity) VALUES ('GeForce GTX 1050 Ti', 'video card', 60000, 40);
INSERT INTO product (p_name, category, price) VALUES ('GeForce GTX 1060', 'video card', 67000);
INSERT INTO product (p_name, category, price, quantity) VALUES ('AMD Ryzen 5 1400 Socket AM4', 'processor', 49000, 13);
INSERT INTO product (p_name, category, price, quantity) VALUES ('AMD Ryzen 5 1600X Socket AM4', 'processor', 73000, 5);
INSERT INTO product (p_name, category, price, quantity) VALUES ('AMD Ryzen 3 1300X AM4', 'processor', 40000, 1);
INSERT INTO product (p_name, category, price, quantity) VALUES ('HyperX Fury 8GB 1600MHz DDR3', 'RAM', 23000, 100);
INSERT INTO product (p_name, category, price, quantity) VALUES ('Kingston HyperX Savage (HXS3) 64GB USB3.1/3.0', 'pendrive', 19200, 23);
INSERT INTO product (p_name, category, price, quantity) VALUES ('HyperX Revolver gamer headset', 'headset', 35990, 13);

CREATE OR REPLACE TABLE buyer (
	id						int(4)			NOT NULL	AUTO_INCREMENT	PRIMARY KEY,
	b_name					varchar(50)		NOT NULL,
	b_address				varchar(50)		NOT NULL,
	date_of_registration	timestamp		NOT NULL	DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO buyer (b_name, b_address) VALUES ('Ray C. Stone', '1597 Bombardier Way Southfield, MI 48075');
INSERT INTO buyer (b_name, b_address, date_of_registration) VALUES ('Steven G. Martin', '4062 Bassel Street Thibodaux, LA 70301', '1999-15-25 12:33:21');
INSERT INTO buyer (b_name, b_address, date_of_registration) VALUES ('Don L. Weber', '4887 Hart Street Bloomfield, CT 06002', '1999-11-10 12:33:21');
INSERT INTO buyer (b_name, b_address, date_of_registration) VALUES ('Donald G. Hart', '2623 Florence Street Denison, TX 75020', '2010-04-11 12:33:21');
INSERT INTO buyer (b_name, b_address, date_of_registration) VALUES ('George C. Browne', '575 Sunny Glen Lane Cleveland, OH 44115', '2017-10-10 12:21:21');
INSERT INTO buyer (b_name, b_address, date_of_registration) VALUES ('Michael R. Rawlings', '3701 Bee Street Muskegon, MI 49440', '2011-11-11 12:33:21');
INSERT INTO buyer (b_name, b_address, date_of_registration) VALUES ('James L. Powell', '4200 Creekside Lane Solvang, CA 93463', '2000-15-25 12:10:21');
INSERT INTO buyer (b_name, b_address, date_of_registration) VALUES ('Dallas J. Burgess', '1163 Rebecca Street Des Plaines, IL 60016', '2001-15-20 12:20:21');

CREATE OR REPLACE TABLE purchases (
	id					int(4)			NOT NULL	AUTO_INCREMENT	PRIMARY KEY,
	product_id			int(4)			NOT NULL,
	buyer_id			int(4)			NOT NULL,
	cost				int(10)			NOT NULL,
	date_of_purchase	timestamp		NOT NULL	DEFAULT CURRENT_TIMESTAMP,
	CONSTRAINT purchases_fk_1 FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT purchases_fk_2 FOREIGN KEY (buyer_id) REFERENCES buyer (id) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO purchases (product_id, buyer_id, cost) VALUES (1, 1, 2000000);
INSERT INTO purchases (product_id, buyer_id, cost, date_of_purchase) VALUES (4, 1, 49000, '1990-05-05 12:00:21');
INSERT INTO purchases (product_id, buyer_id, cost, date_of_purchase) VALUES (9, 1, 35990, '2000-10-15 12:33:21');
INSERT INTO purchases (product_id, buyer_id, cost, date_of_purchase) VALUES (7, 2, 23000, '2001-10-15 12:33:21');
INSERT INTO purchases (product_id, buyer_id, cost, date_of_purchase) VALUES (7, 2, 23000, '2010-01-20 12:33:21');
INSERT INTO purchases (product_id, buyer_id, cost, date_of_purchase) VALUES (5, 3, 73000, '1999-15-25 12:33:21');

-- shows first two most purchased products
CREATE OR REPLACE VIEW most_purchased_products (product,amount_purchased) AS
SELECT p_name, COUNT(*) FROM product INNER JOIN purchases ON product.id = product_id
GROUP BY p_name
ORDER BY COUNT(*) DESC
LIMIT 2;

-- shows which products are out of stock
CREATE OR REPLACE VIEW out_of_stock (product) AS
SELECT p_name FROM product WHERE quantity = 0;

-- shows first two buyer with the most purchases
CREATE OR REPLACE VIEW popular_buyers (buyer,number_of_purchases) AS
SELECT b_name, COUNT(*) FROM buyer INNER JOIN purchases ON buyer.id = buyer_id
GROUP BY b_name
ORDER BY COUNT(*) DESC
LIMIT 2;