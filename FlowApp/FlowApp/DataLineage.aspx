﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage.Master" AutoEventWireup="true" CodeBehind="DataLineage.aspx.cs" Inherits="FlowApp.DataLineage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="css/stylesheet.css" type="text/css"/>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <div align="center">
        <table class="configTable">
            <tr id="hostRow" runat="server">
                <td>Host:</td>
                <td><asp:Label ID="hostname" runat="server"></asp:Label></td>
            </tr>
            <tr id="databaseRow" runat="server">
                <td>Database:</td>
                <td><asp:DropDownList ID="databaseNames" runat="server"></asp:DropDownList></td>
            </tr>
            <tr id="viewFilterRow" runat="server">
                <td>Filter view:</td>
                <td><asp:DropDownList ID="viewFilter" runat="server"></asp:DropDownList></td>
            </tr>
        </table>
        <asp:Button ID="submitButton" runat="server" Text="Submit"/>
    </div>

    <br />
    <asp:Panel ID="DataLineagePanel" runat="server" CssClass="divrow">
        <asp:Panel ID="TitleRow" runat="server" CssClass="titlerow">

        </asp:Panel>
        <asp:Panel ID="DivRow" runat="server">
        
        </asp:Panel>
    </asp:Panel>
    <asp:Panel ID="LogRow" CssClass="logrow" runat="server">
        
    </asp:Panel>
</asp:Content>
