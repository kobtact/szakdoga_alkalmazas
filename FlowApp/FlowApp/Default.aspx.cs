﻿using FlowAppCore.Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using static FlowAppCore.Types.Datatypes;

namespace FlowApp
{
    public partial class Default : Page
    {
        private const string SESSION_KEY_DATABASE_CONTROLLER = "DatabaseController";
        private const string SESSION_KEY_CONNECTION_CONTROLLER = "ConnectionController";
        private const string SESSION_KEY_HOSTNAME = "Hostname";
        private const string SESSION_KEY_DATABASENAME = "Databasename";

        // property-k a Session objektumokhoz
        protected DatabaseController DatabaseController
        {
            get
            {
                if (Session[SESSION_KEY_DATABASE_CONTROLLER] == null)
                {
                    DatabaseController controller = new DatabaseController();
                    Session[SESSION_KEY_DATABASE_CONTROLLER] = controller;
                }
                return (DatabaseController)Session[SESSION_KEY_DATABASE_CONTROLLER];
            }
        }

        protected ConnectionController ConnectionController
        {
            get
            {
                return (ConnectionController)Session[SESSION_KEY_CONNECTION_CONTROLLER];
            }
        }

        protected string Hostname
        {
            get
            {
                return (string)Session[SESSION_KEY_HOSTNAME];
            }
        }

        protected string Databasename
        {
            get
            {
                return (string)Session[SESSION_KEY_DATABASENAME];
            }
            set
            {
                Session[SESSION_KEY_DATABASENAME] = value;
            }
        }

        private void Page_Load(object sender, EventArgs e)
        {
            if(IsPostBack)
            {
                try
                {
                    ConnectionController connectionController = new ConnectionController(host.Value, port.Value, username.Value, password.Value);
                    if (connectionController.IsConnectionAlive())
                    {
                        // új kapcsolat objektum, és adatbázis nevek SESSION - be
                        Session[SESSION_KEY_CONNECTION_CONTROLLER] = connectionController;
                        Session[SESSION_KEY_HOSTNAME] = host.Value;
                        Databasename = "none";

                        //ShowMessage("Sikeres csatlakozás!");

                        Response.Redirect("DataLineage.aspx");
                    }
                    else
                    {
                        ShowMessage("Nem sikerült csatlakozni!");
                    }
                } catch(Exception ex)
                {
                    ShowMessage(ex.Message);
                }
                
                
            }
        }

        /*
         *  Figyelmeztető üzenet felugró ablakban
         */
        protected void ShowMessage(string message)
        {
            Response.Write("<script type='text/javascript'>alert('" + message + "')</script>");
        }
    }
}