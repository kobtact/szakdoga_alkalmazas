﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="FlowApp.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="css/stylesheet.css" type="text/css"/>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div align="center">
        <table class="configTable">
            <tr id="hostRow" runat="server">
                <td>Host:</td>
                <td><input type="text" id="host" runat="server" value="localhost"></input></td>
            </tr>
            <tr id="portRow" runat="server">
                <td>Port:</td>
                <td><input type="number" id="port" runat="server" value="3306"></input></td>
            </tr>
            <tr id="userRow" runat="server">
                <td>Username:</td>
                <td><input type="text" id="username" runat="server" value="root"></input></td>
            </tr>
            <tr id="passwordRow" runat="server">
                <td>Password</td>
                <td><input type="password" id="password" runat="server"></input></td>
            </tr>
        </table>
        <asp:Button ID="submitButton" runat="server" Text="Submit"/>
    </div>
</asp:Content>


