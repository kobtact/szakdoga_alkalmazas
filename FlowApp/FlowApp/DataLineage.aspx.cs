﻿using FlowAppCore.Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using static FlowAppCore.Types.Datatypes;

namespace FlowApp
{
    public partial class DataLineage : Default
    {
        List<TableConnection> foreignKeyRelations = null;
        private HashSet<TableConnection> tableViewRelations = null;
        private Dictionary<string, HashSet<string>> relatedTables = null;
        private List<string> views = null;

        // az itt definiált segédfüggvényekkel eltároljuk az egyes attribútumokhoz
        // és nézetekhez tartozó attribútumok listáját
        private Dictionary<string, List<string>> attributeRelatedAttributes = new Dictionary<string, List<string>>();
        private Dictionary<string, List<string>> viewRelatedAttributes = new Dictionary<string, List<string>>();

        // itt tároljuk a táblák mélységének meghatározása során az eddig már látott táblákhoz
        // tartozó mélységeket
        private Dictionary<string, int> tableDepth = new Dictionary<string, int>();

        protected void Page_Init(object sender, EventArgs e)
        {
            if (ConnectionController.IsConnectionAlive())
            {
                if (hostname.Text != Hostname)
                {
                    hostname.Text = Hostname;
                    databaseNames.DataSource = ConnectionController.GetAllDatabases();
                    databaseNames.DataBind();
                }
            }
            else
            {
                ShowMessage("Megszakadt a kapcsolat!");
                Response.Redirect("Default.aspx");
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ConnectionController.UpdateConnectionWithDatabase(databaseNames.SelectedValue);

            DatabaseController.UpdateDatabase(ConnectionController.GetConnection(), databaseNames.SelectedValue);

            foreignKeyRelations = DatabaseController.GetForeignKeyRelations();
            tableViewRelations = DatabaseController.GetViewRelations();

            relatedTables = DatabaseController.GetRelatedTables();
            views = DatabaseController.GetViewNames();

            if (databaseNames.SelectedValue != Databasename)
            {
                Databasename = databaseNames.SelectedValue;

                viewFilter.DataSource = views;
                viewFilter.DataBind();
                viewFilter.Items.Insert(0, "none");
            }

            // csak akkor hozzuk létre a táblákat, ha sikeres volt a csatlakozás
            if (ConnectionController.IsConnectionAlive())
            {
                createTables(viewFilter.SelectedValue);
            }
            else
            {
                ShowMessage("Sikertelen csatlakozás!");
            }

            // ha volt postback
            if (IsPostBack)
            {

                // TODO : új szűrés

                // ha volt klikk (tehát volt postback), akkor átszínezzük a cellákat, ahol szerepel a választott
                // nevű cella, a többitől eltávolítjuk ezt a classt, ha volt

                if (Request.QueryString.HasKeys())
                {
                    string selected = Request.QueryString["selected"];
                    List<string> selectedAttributes = selected.Split(',').ToList();

                    Dictionary<string, List<string>> relatedAttributes = attributeRelatedAttributes;
                    viewRelatedAttributes.ToList().ForEach(x => relatedAttributes.Add(x.Key, x.Value));

                    #region reláció log
                    // LOG rész megjelenítése
                    // szöveges formázott üzenet a kapcsolatokról

                    // először eldöntjük a választott elem alapján, hogy nézettel vagy tábla attribútummal van dolgunk
                    Label SelectedLabel = new Label()
                    {
                        CssClass = "labelStyle"
                    };
                    Panel panel = new Panel()
                    {
                        CssClass = "divcolumn",
                        HorizontalAlign = HorizontalAlign.Center
                    };
                    string[] separate = selectedAttributes.First().Split('.');

                    // tábla vagy nézet neve
                    SelectedLabel.Text = "<h1><i>" + separate[0] + "</i></h1><h2>";

                    // ha a tömbbe csak egy elem került a szétválasztás után, akkor az biztosan nézet lesz
                    if (separate.Length == 1)
                    {
                        SelectedLabel.Text += " nézet adateredet relációi</h2>";
                        panel.Controls.Add(SelectedLabel);
                        LogRow.Controls.Add(panel);

                        foreach (string table in relatedTables.Keys)
                        {

                            panel = new Panel();

                            Label label = new Label()
                            {
                                Text = "<h4>" + table + " tábla</h4>",
                                CssClass = "labelStyle"
                            };

                            foreach (string attribute in relatedTables[table])
                            {
                                foreach (TableConnection relation in tableViewRelations)
                                {
                                    if (relation.firstTable == selectedAttributes.First() &&
                                        table + "." + attribute == relation.secondTable + "." + relation.secondCol)
                                    {
                                        label.Text += "<p> &rarr; kapcsolat <i>" + attribute + "</i> attribútummal</p>";

                                        foreach (TableConnection fkRelation in foreignKeyRelations)
                                        {
                                            if (table + "." + attribute == fkRelation.firstTable + "." + fkRelation.firstCol)
                                            {
                                                label.Text += "<p>&nbsp&nbsp&nbsp &rarr;" + " <i>külső kulcs</i> &rarr; <i>" + fkRelation.secondTable + "</i> tábla <i>" + fkRelation.secondCol + "</i> oszlop referencia</p>";
                                            }
                                            if (table + "." + attribute == fkRelation.secondTable + "." + fkRelation.secondCol)
                                            {
                                                label.Text += "<p>&nbsp&nbsp&nbsp &rarr;" + " <i>referencia</i> &rarr; <i>" + fkRelation.firstTable + "</i> tábla <i>" + fkRelation.firstCol + "</i> külső kulcsára</p>";
                                            }
                                        }
                                    }
                                }
                            }

                            if (label.Text != "<h4>" + table + " tábla</h4>")
                            {
                                panel.Controls.Add(label);
                                LogRow.Controls.Add(panel);
                            }
                        }
                    }
                    // egyébként tábláról van szó, kiválasztott attribútumonként kilistázzuk a kapcsolódásokat
                    else
                    {
                        SelectedLabel.Text += " tábla adateredet relációi</h2>";
                        panel.Controls.Add(SelectedLabel);
                        LogRow.Controls.Add(panel);

                        foreach (string selectedAttribute in selectedAttributes)
                        {

                            // attribútum neve a "fejléc"
                            string selectAttrSplit = selectedAttribute.Split('.')[1];

                            panel = new Panel();

                            Label label = new Label()
                            {
                                Text = "<h4>" + selectAttrSplit + "</h4>",
                                CssClass = "labelStyle"
                            };

                            foreach (TableConnection fkRelation in foreignKeyRelations)
                            {
                                if (selectedAttribute == fkRelation.firstTable + "." + fkRelation.firstCol)
                                {
                                    label.Text += "<p> &rarr;" + " <i>külső kulcs</i> &rarr; <i>" + fkRelation.secondTable + "</i> tábla <i>" + fkRelation.secondCol + "</i> oszlop referencia" + "</p>";
                                }
                                if (selectedAttribute == fkRelation.secondTable + "." + fkRelation.secondCol)
                                {
                                    label.Text += "<p> &rarr;" + " <i>referencia</i> &rarr; <i>" + fkRelation.firstTable + "</i> tábla <i>" + fkRelation.firstCol + "</i> külső kulcsára" + "</p>";
                                }
                            }

                            foreach (TableConnection viewRelation in tableViewRelations)
                            {
                                if (selectedAttribute == viewRelation.secondTable + "." + viewRelation.secondCol)
                                {
                                    label.Text += "<p> &rarr;" + " kapcsolat <i>" + viewRelation.firstTable + "</i> nézettel" + "</p>";
                                }
                            }
                            panel.Controls.Add(label);
                            LogRow.Controls.Add(panel);
                        }
                    }
                    #endregion

                    #region reláció kiemelése
                    // kiválasztott cella adateredetének kiemelése
                    foreach (Control div in DivRow.Controls)
                    {
                        foreach (WebControl table in div.Controls)
                        {
                            foreach (WebControl row in table.Controls)
                            {
                                foreach (WebControl cell in row.Controls)
                                {
                                    if (cell.ID != null)
                                    {
                                        // ha az aktuális cella kiválasztott vagy kapcsolódó elem, kiemeljük
                                        // a megfelelő színnel, egyébként eltávolítjuk ezeket az osztályokat róla
                                        foreach (string selectedAttribute in selectedAttributes)
                                        {
                                            if (cell.ID == selectedAttribute)
                                            {
                                                cell.CssClass = "cellSelected";
                                                break;
                                            }
                                            else if (relatedAttributes.ContainsKey(selectedAttribute))
                                            {
                                                if (relatedAttributes[selectedAttribute].Contains(cell.ID))
                                                {
                                                    cell.CssClass = "cellRelated";
                                                    break;
                                                }
                                            }
                                            cell.CssClass = "";
                                        }
                                    }
                                }
                            }
                        }
                    }
                    #endregion
                }
            }
        }

        /// <summary>
        /// táblák dinamikus létrehozása
        /// </summary>
        /// <param name="viewFilter">a nézet neve, amire szűrjük az adateredet diagram elemeit</param>
        private void createTables(string viewFilter)
        {
            // a megjelenített adat eredet elemek:
            // - külső kulcsok és kapcsolódó tábláik
            // - nézetek és kapcsolódó táblák

            Table table;
            TableHeaderRow hr;
            TableRow tr;
            TableCell td;
            TableHeaderCell hcell;
            // ez az egyik módja a cella klikk eseménynek ASP.NET - ben
            LinkButton selectButton;

            // előre létrehozzuk a paneleket a táblák becsült elrendezéséhez

            // ELRENDEZÉS - TitleRow Panelben

            // egyszerű táblák panelek balra
            Panel titlePanel = new Panel()
            {
                CssClass = "divcolumn"
            };
            titlePanel.Controls.Add(new Label() { Text = "Simple Tables" });
            TitleRow.Controls.Add(titlePanel);

            Panel divPanel = new Panel()
            {
                CssClass = "divcolumn",
                ID = "SimpleTableColumn"
            };
            DivRow.Controls.Add(divPanel);

            // külső kulcsos panelek középre
            ForeignKeyPanels();

            // nézet panelek jobbra
            titlePanel = new Panel()
            {
                CssClass = "divcolumn"
            };
            titlePanel.Controls.Add(new Label() { Text = "Views" });
            TitleRow.Controls.Add(titlePanel);

            divPanel = new Panel()
            {
                CssClass = "divcolumn",
                ID = "ViewColumn"
            };
            DivRow.Controls.Add(divPanel);

            // TÁBLÁK - DivRow Panelben
            foreach (KeyValuePair<string, HashSet<string>> relatedTable in relatedTables)
            {
                table = new Table()
                {
                    ID = relatedTable.Key
                };

                hr = new TableHeaderRow();
                hcell = new TableHeaderCell()
                {
                    Text = relatedTable.Key
                };

                selectButton = new LinkButton()
                {
                    Text = relatedTable.Key
                };

                // kiválasztjuk az összes sztringet, ha a tábla nevére klikkelnek
                string selectAttributes = "";
                foreach (string attribute in relatedTable.Value)
                {
                    selectAttributes += relatedTable.Key + "." + attribute + ",";
                }
                selectAttributes = selectAttributes.TrimEnd(',');

                selectButton.PostBackUrl = Request.Url.AbsolutePath + "?selected=" + selectAttributes;

                hcell.Controls.Add(selectButton);

                hr.Controls.Add(hcell);

                table.Controls.Add(hr);

                foreach (string attribute in relatedTable.Value)
                {
                    // mélység számítása az aktuális attribútumra
                    // ha nagyobb mint a tábla aktuális mélysége, ez lesz az új mélység
                    // ShowMessage(relatedTable.Key + "." + attribute + " : " + ForeignKeyDepthOfAttribute(relatedTable.Key, attribute).ToString());

                    string attr = relatedTable.Key + "." + attribute;
                    tr = new TableRow();
                    td = new TableCell()
                    {
                        ID = attr,
                        Text = attribute
                    };

                    if (!attributeRelatedAttributes.ContainsKey(attr))
                    {
                        attributeRelatedAttributes.Add(attr, RelationsForAttributes(attr));
                        // itt már tudjuk, mi kapcsolódik az aktuális attribútumhoz, és még nem adtunk hozzá elemet
                        // ha nincs közte a nézet, tovább lépünk a ciklusban még mielőtt hozzáadnánk a honlaphoz
                        if (viewFilter != "none" && !attributeRelatedAttributes[attr].Contains(viewFilter))
                        {
                            continue;
                        }
                    }

                    selectButton = new LinkButton()
                    {
                        Text = attribute
                    };
                    selectButton.PostBackUrl = Request.Url.AbsolutePath + "?selected=" + relatedTable.Key + "." + attribute;
                    //selectButton.Click += new EventHandler(Cell_Click);

                    td.Controls.Add(selectButton);

                    tr.Controls.Add(td);

                    table.Controls.Add(tr);
                }

                // ha üres a tábla, ne jelenítsük meg
                if(table.Controls.Count == 1)
                {
                    continue;
                }

                int depth = ForeignKeyDepthOfTable(relatedTable.Key);
                
                if (depth == 0)
                {
                    foreach (Control control in DivRow.Controls)
                    {
                        if (control.ID == "SimpleTableColumn")
                        {
                            control.Controls.Add(table);
                        }
                    }
                }
                else if (depth > 0)
                {
                    foreach(Control control in DivRow.Controls)
                    {
                        if(control.ID == "ForeignTablePanel" + depth)
                        {
                            control.Controls.Add(table);
                        }
                    }
                }
                

            }

            // NÉZETEK - DivRow Panelben
            table = new Table();

            hr = new TableHeaderRow();
            hcell = new TableHeaderCell()
            {
                Text = "Views"
            };
            hr.Controls.Add(hcell);

            table.Controls.Add(hr);

            foreach (string view in views)
            {
                if (viewFilter != "none" && viewFilter != view)
                {
                    continue;
                }

                tr = new TableRow();
                td = new TableCell()
                {
                    ID = view,
                    Text = view
                };

                if (!viewRelatedAttributes.ContainsKey(view))
                {
                    viewRelatedAttributes.Add(view, RelationsForView(view));
                }

                selectButton = new LinkButton()
                {
                    Text = view
                };
                selectButton.PostBackUrl = Request.Url.AbsolutePath + "?selected=" + view;
                td.Controls.Add(selectButton);

                tr.Controls.Add(td);

                table.Controls.Add(tr);
            }

            if(table.Controls.Count == 1)
            {
                tr = new TableRow();
                td = new TableCell()
                {
                    Text = "Nincs nézet az adatbázisban!"
                };
                tr.Controls.Add(td);
                table.Controls.Add(tr);
            }

            foreach (Control control in DivRow.Controls)
            {
                if (control.ID == "ViewColumn")
                {
                    control.Controls.Add(table);
                }
            }

            // végül az üres oszlopokat eltüntetjük ( csak ha volt kattintás )
            if (IsPostBack)
            {
                foreach (Control control in DivRow.Controls)
                {
                    if (!control.HasControls())
                    {
                        control.Visible = false;
                        TitleRow.Controls[DivRow.Controls.IndexOf(control)].Visible = false;
                    } else
                    {
                        control.Visible = true;
                        TitleRow.Controls[DivRow.Controls.IndexOf(control)].Visible = true;
                    }
                }
            }
            
            
        }
        /// <summary>
        /// rekurzív függvény az attribútum mélység meghatározásához
        /// </summary>
        /// <param name="table">tábla neve</param>
        /// <param name="attribute">táblában lévő attribútum neve</param>
        /// <returns></returns>
        private int ForeignKeyDepthOfAttribute(string table, string attribute)
        {
            int depth = 0;
            foreach(TableConnection fkRelation in foreignKeyRelations)
            {
                // ha az aktuális tábla és attribútum nem külső kulcs, csak referencia, végére értünk
                if(fkRelation.firstTable == null && fkRelation.firstCol == null && fkRelation.secondTable == table && fkRelation.secondCol == attribute)
                {
                    return depth + 1;
                }
                else if (fkRelation.firstTable == table && fkRelation.firstCol == attribute && fkRelation.secondTable != null)
                {
                    depth = 1 + ForeignKeyDepthOfTable(fkRelation.secondTable);   
                }
            }

            return depth;
        }

        /// <summary>
        /// attribútum mélység táblára
        /// </summary>
        /// <param name="table">tábla neve</param>
        /// <returns></returns>
        private int ForeignKeyDepthOfTable(string table)
        {
            int maxDepth = 0;
            HashSet<string> currentTableAttributes = relatedTables[table];
            foreach (string attribute in currentTableAttributes)
            {
                int depth = ForeignKeyDepthOfAttribute(table, attribute);
                if (depth > maxDepth)
                {
                    maxDepth = depth;
                }
                if(maxDepth > 2)
                {
                    return maxDepth;
                }
            }
            
            return maxDepth;
        }

        /// <summary>
        /// visszaadja a külső kulcsos táblák becsült elrendezéséhez
        /// a szükséges Panel objektumokat
        /// </summary>
        /// <returns></returns>
        private void ForeignKeyPanels()
        {
            int maxDepth = 0;

            foreach (string table in relatedTables.Keys.ToList())
            {
                int depth = ForeignKeyDepthOfTable(table);

                // tábla mélységét kiszámoljuk, és elmentjük, hogy újra ne kelljen
                // kiszámolni a későbbi tábláknál
                tableDepth.Add(table, depth);

                if (depth > maxDepth)
                {
                    maxDepth = depth;
                }
            }

            for (int i = 1; i <= maxDepth; i++)
            {
                Panel panel = new Panel()
                {
                    CssClass = "divcolumn",
                };
                panel.Controls.Add(new Label() { Text = "ForeignTableColumn" + i } );
                TitleRow.Controls.Add(panel);

                panel = new Panel()
                {
                    ID = "ForeignTablePanel" + i,
                    CssClass = "divcolumn"
                };
                DivRow.Controls.Add(panel);
            }
        }

        /// <summary>
        /// a nézetek nevéhez milyen attribútumok kapcsolódnak
        /// </summary>
        /// <param name="viewName"></param>
        /// <returns></returns>
        private List<string> RelationsForView(string viewName)
        {
            HashSet<string> relations = new HashSet<string>();

            foreach (TableConnection fkRelation in foreignKeyRelations)
            {
                if (fkRelation.firstTable == viewName)
                {
                    relations.Add(fkRelation.secondTable + "." + fkRelation.secondCol);
                }
                else if (fkRelation.secondTable == viewName)
                {
                    relations.Add(fkRelation.firstTable + "." + fkRelation.firstCol);
                }
            }

            foreach (TableConnection viewRelation in tableViewRelations)
            {
                if (viewRelation.firstTable == viewName)
                {
                    relations.Add(viewRelation.secondTable + "." + viewRelation.secondCol);
                }
                else if (viewRelation.secondTable == viewName)
                {
                    relations.Add(viewRelation.firstTable);
                }
            }

            return relations.ToList();
        }

        

        /// <summary>
        /// attribútumok relációinak megkeresése
        /// </summary>
        /// <param name="attributeName"></param>
        /// <returns></returns>
        private List<string> RelationsForAttributes(string attributeName)
        {
            HashSet<string> relations = new HashSet<string>();

            foreach (TableConnection fkRelation in foreignKeyRelations)
            {
                if (fkRelation.firstTable + "." + fkRelation.firstCol == attributeName)
                {
                    relations.Add(fkRelation.secondTable + "." + fkRelation.secondCol);
                }
                else if (fkRelation.secondTable + "." + fkRelation.secondCol == attributeName)
                {
                    relations.Add(fkRelation.firstTable + "." + fkRelation.firstCol);
                }
            }

            foreach (TableConnection viewRelation in tableViewRelations)
            {
                if (viewRelation.secondTable + "." + viewRelation.secondCol == attributeName)
                {
                    relations.Add(viewRelation.firstTable);
                }
            }

            return relations.ToList();

        }
    }
}