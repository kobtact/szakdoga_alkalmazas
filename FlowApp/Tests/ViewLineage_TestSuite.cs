﻿using NUnit.Framework;
using System;
using System.Text;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;

namespace Tests
{
    [TestFixture]
    public class ViewLineage_TestSuite
    {
        private IWebDriver driver;
        private StringBuilder verificationErrors;
        private string baseURL;
        private bool acceptNextAlert = true;

        [OneTimeSetUp]
        public void SetupTest()
        {
            driver = new FirefoxDriver();
            baseURL = "https://www.katalon.com/";
            verificationErrors = new StringBuilder();
        }

        [OneTimeTearDown]
        public void TeardownTest()
        {
            try
            {
                driver.Quit();
            }
            catch (Exception)
            {
                // Ignore errors if unable to close the browser
            }
            Assert.AreEqual("", verificationErrors.ToString());
        }

        [Test]
        public void ViewLineage_Test001()
        {
            driver.Navigate().GoToUrl("http://localhost/alkalmazas/");
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.Fail("timeout");
                try
                {
                    if (IsElementPresent(By.Id("ContentPlaceHolder1_submitButton"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.Id("ContentPlaceHolder1_submitButton")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.Fail("timeout");
                try
                {
                    if (IsElementPresent(By.Id("ContentPlaceHolder1_databaseNames"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
        }

        [Test]
        public void ViewLineage_Test002()
        {
            driver.FindElement(By.Id("ContentPlaceHolder1_databaseNames")).Click();
            new SelectElement(driver.FindElement(By.Id("ContentPlaceHolder1_databaseNames"))).SelectByText("testdatabase");
            driver.FindElement(By.Id("ContentPlaceHolder1_databaseNames")).Click();
            driver.FindElement(By.Id("ContentPlaceHolder1_submitButton")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.Fail("timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("most_purchased_products"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
        }

        [Test]
        public void ViewLineage_Test003()
        {
            driver.FindElement(By.LinkText("most_purchased_products")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.Fail("timeout");
                try
                {
                    if (IsElementPresent(By.XPath("//div[@id='ContentPlaceHolder1_LogRow']/div/span/h1/i"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            Assert.AreEqual("most_purchased_products", driver.FindElement(By.XPath("//div[@id='ContentPlaceHolder1_LogRow']/div/span/h1/i")).Text);
            Assert.AreEqual("nézet adateredet relációi", driver.FindElement(By.XPath("//div[@id='ContentPlaceHolder1_LogRow']/div/span/h2")).Text);
        }

        private bool IsElementPresent(By by)
        {
            try
            {
                driver.FindElement(by);
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }

        private bool IsAlertPresent()
        {
            try
            {
                driver.SwitchTo().Alert();
                return true;
            }
            catch (NoAlertPresentException)
            {
                return false;
            }
        }

        private string CloseAlertAndGetItsText()
        {
            try
            {
                IAlert alert = driver.SwitchTo().Alert();
                string alertText = alert.Text;
                if (acceptNextAlert)
                {
                    alert.Accept();
                }
                else
                {
                    alert.Dismiss();
                }
                return alertText;
            }
            finally
            {
                acceptNextAlert = true;
            }
        }
    }
}

