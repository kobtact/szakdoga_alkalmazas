﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;

namespace Tests
{
    [TestFixture]
    public class InvalidLogin_TestSuite
    {
        private IWebDriver driver;
        private StringBuilder verificationErrors;
        private string baseURL;
        private bool acceptNextAlert = true;

        [SetUp]
        public void SetupTest()
        {
            driver = new FirefoxDriver();
            baseURL = "https://www.katalon.com/";
            verificationErrors = new StringBuilder();
        }

        [TearDown]
        public void TeardownTest()
        {
            try
            {
                driver.Quit();
            }
            catch (Exception)
            {
                // Ignore errors if unable to close the browser
            }
            Assert.AreEqual("", verificationErrors.ToString());
        }

        [Test]
        public void InvalidLogin_Test001()
        {
            driver.Navigate().GoToUrl("http://localhost/alkalmazas/");
            driver.FindElement(By.Id("ContentPlaceHolder1_host")).Click();
            driver.FindElement(By.Id("ContentPlaceHolder1_host")).Clear();
            driver.FindElement(By.Id("ContentPlaceHolder1_host")).SendKeys("fgsdfgléjsdg");
            driver.FindElement(By.Id("ContentPlaceHolder1_port")).Clear();
            driver.FindElement(By.Id("ContentPlaceHolder1_port")).SendKeys("1111");
            driver.FindElement(By.Id("ContentPlaceHolder1_username")).Clear();
            driver.FindElement(By.Id("ContentPlaceHolder1_username")).SendKeys("gfdgn");
            driver.FindElement(By.Id("ContentPlaceHolder1_password")).Clear();
            driver.FindElement(By.Id("ContentPlaceHolder1_password")).SendKeys("nlkfdng");
            driver.FindElement(By.Id("ContentPlaceHolder1_submitButton")).Click();
            Assert.AreEqual("Nem sikerült csatlakozni!", CloseAlertAndGetItsText());
        }
        private bool IsElementPresent(By by)
        {
            try
            {
                driver.FindElement(by);
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }

        private bool IsAlertPresent()
        {
            try
            {
                driver.SwitchTo().Alert();
                return true;
            }
            catch (NoAlertPresentException)
            {
                return false;
            }
        }

        private string CloseAlertAndGetItsText()
        {
            try
            {
                IAlert alert = driver.SwitchTo().Alert();
                string alertText = alert.Text;
                if (acceptNextAlert)
                {
                    alert.Accept();
                }
                else
                {
                    alert.Dismiss();
                }
                return alertText;
            }
            finally
            {
                acceptNextAlert = true;
            }
        }
    }
}
