﻿using FlowAppCore.DAO;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlowAppCore.Controller
{
    public class ConnectionController
    {
        private readonly string HOST;
        private readonly string PORT;
        private readonly string USERNAME;
        private readonly string PASSWORD;

        private string ConnectionString
        {
            get
            {
                return "datasource=" + HOST + ";" +
                        "port=" + PORT + ";" +
                        "username=" + USERNAME + ";" +
                        "password=" + PASSWORD + ";";
            }
        }

        /// <summary>
        /// MySQL kapcsolat objektum, átadjuk a DatabaseControllernek
        /// </summary>
        private MySqlConnection connection = null;

        /// <summary>
        /// adatbázis nevek listája
        /// </summary>
        private List<string> databaseNames = null;

        private ConnectionMethods ConnectionMethods = new ConnectionMethods();

        /// <summary>
        /// inicializáljuk a kezdeti csatlakozás objektumot
        /// </summary>
        /// <param name="host">host neve vagy címe</param>
        /// <param name="port">csatlakozáshoz használt portszám</param>
        /// <param name="username">felhasználónév</param>
        /// <param name="password">jelszó</param>
        /// <param name="database">adatbázis neve, opcionális, mivel először csak a hosthoz csatlakozunk</param>
        public ConnectionController(string host, string port, string username, string password, string database="")
        {
            HOST = host;
            PORT = port;
            USERNAME = username;
            PASSWORD = password;
            connection = new MySqlConnection(ConnectionString);
            try
            {
                connection.Open();
                databaseNames = ConnectionMethods.GetAllDatabaseNames(connection);
            }
            catch (MySqlException)
            {
                Console.WriteLine("Sikertelen csatlakozás " + HOST + " címre!");
            }
        }

        ~ConnectionController()
        {
            connection.Close();
        }

        /// <summary>
        /// visszaadja az összes adatbázis nevét, ezeket a frontend megkapja
        /// hogy a felhasználó kiválaszthassa melyiken szeretné futtatni az alkalmazást
        /// </summary>
        /// <returns></returns>
        public List<string> GetAllDatabases()
        {
            return databaseNames;
        }

        /// <summary>
        /// visszaadja a kapcsolat objektumot
        /// </summary>
        /// <returns></returns>
        public MySqlConnection GetConnection()
        {
            return connection;
        }

        /// <summary>
        /// visszajelzés ha nem jó a connection, frontendnek hasznos
        /// </summary>
        /// <returns></returns>
        public bool IsConnectionAlive()
        {
            return connection.Ping();
        }

        /// <summary>
        /// a paraméterben megadott adatbázisra állítja a kapcsolat objektumot
        /// </summary>
        /// <param name="databaseName">adatbázis neve</param>
        /// <returns>sikeres volt-e</returns>
        public bool UpdateConnectionWithDatabase(string databaseName)
        {
            bool updateSucc = ConnectionMethods.UpdateConnectionWithDatabase(connection, databaseName);

            return updateSucc;
        }
    }
}
