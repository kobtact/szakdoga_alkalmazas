﻿using FlowAppCore.DAO;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static FlowAppCore.Types.Datatypes;

namespace FlowAppCore.Controller
{
    public class DatabaseController
    {

        private SchemaMethods SchemaMethods { get; set; }

        // már a controllerben lementjük az adatbázissal kapcsolatos adatokat, mivel a megjelenítéshez
        // ezeket már többször nem kell lekérdezzük (így gyorsabb is lesz végeredményben)
        private List<TableConnection> foreignKeyRelations = null;
        private HashSet<TableConnection> tableViewRelations = null;
        private Dictionary<string, HashSet<string>> relatedTables = null;
        private List<string> views = null;

        // csak debuggoláshoz
        public List<Tuple<string, string>> viewDefinitions = null;

        public List<Tuple<string, string>> GetAllViewDefinition(MySqlConnection connection, string database)
        {
            return SchemaMethods.GetAllViewDefinition(connection, database);
        }

        /// <summary>
        /// az aktuális kapcsolathoz tartozó, a felhasználó által választott
        /// adatbázis neve alapján frissíti a séma információt az adatbázisról
        /// </summary>
        /// <param name="connection">csatlakozás objektum</param>
        /// <param name="databaseName">adatbázis neve</param>
        public void UpdateDatabase (MySqlConnection connection, string databaseName)
        {
            try
            {
                SchemaMethods = new SchemaMethods(connection, databaseName);
                foreignKeyRelations = SchemaMethods.ForeignKeyConnections(connection, databaseName);
                tableViewRelations = SchemaMethods.TableRelations(connection, databaseName);
                relatedTables = RelatedTables();
                views = SchemaMethods.GetAllViewNames(connection, databaseName);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public List<string> GetViewNames()
        {
            return views;
        }

        public HashSet<TableConnection> GetViewRelations()
        {
            return tableViewRelations;
        }

        public List<TableConnection> GetForeignKeyRelations()
        {
            return foreignKeyRelations;
        }

        /// <summary>
        /// az alapvető táblák (nem nézetek) az összes kapcsolódó attirbútummal
        /// </summary>
        /// <returns> szótár táblanév kulcssal, és attribútum listával </returns>
        public Dictionary<string, HashSet<string>> GetRelatedTables()
        {
            return relatedTables;
        }

        #region segédfüggvények




        /// <summary>
        ///  a kapcsolódó táblák alapján kigyűjtjük a külső kulcsokat tartalmazókat
        ///  a dictionary kulcsa a tábla neve, a listában a kapcsolódó attribútumok
        /// </summary>
        /// <returns> táblák és attribútumaik </returns>

        private Dictionary<string, HashSet<string>> TablesWithFKAttributes()
        {
            Dictionary<string, HashSet<string>> result = new Dictionary<string, HashSet<string>>();

            foreach (TableConnection relation in foreignKeyRelations)
            {
                // ha nem szerepelt még ilyen tábla név az eredmény listában
                // hozzáadja a külső kulccsal együtt
                if (!result.ContainsKey(relation.firstTable))
                {
                    HashSet<string> columns = new HashSet<string>();
                    columns.Add(relation.firstCol);
                    result.Add(relation.firstTable, columns);
                }
                // egyébként hozzáadja a megfelelő kulcsú HashSet-hez (vagy nem, ha már szerepelt benne)
                else
                {
                    result[relation.firstTable].Add(relation.firstCol);
                }

                // ugyanígy a referált táblára is
                if (!result.ContainsKey(relation.secondTable))
                {
                    HashSet<string> columns = new HashSet<string>();
                    columns.Add(relation.secondCol);
                    result.Add(relation.secondTable, columns);
                }
                else
                {
                    result[relation.secondTable].Add(relation.secondCol);
                }
            }
            return result;
        }
        /// <summary>
        /// megkapja a külső kulcsos szótárat és kibővíti a nézet alapján kapcsolódó attribútumokkal
        /// vagy egyéb csak a nézettel kapcsolódó táblával és attribútummal
        /// </summary>
        /// <param name="fkRelatedTables"> külső kulcsos táblák, és kapcsolódó attribútumaik </param>
        /// <returns></returns>
        private Dictionary<string, HashSet<string>> TablesWithViewAttributes(Dictionary<string, HashSet<string>> fkRelatedTables)
        {
            foreach(TableConnection relation in tableViewRelations)
            {
                // itt csak egy if-else ág van, mivel a TableConnection-nél
                // a firstCol üres, a firstTable csak a nézet nevét tartalmazza
                // így csak egy kapcsolódást kell megnézni
                if (!fkRelatedTables.ContainsKey(relation.secondTable))
                {
                    HashSet<string> columns = new HashSet<string>();
                    columns.Add(relation.secondCol);
                    fkRelatedTables.Add(relation.secondTable, columns);
                } else
                {
                    fkRelatedTables[relation.secondTable].Add(relation.secondCol);
                }
            }
            return fkRelatedTables;
        }

        /// <summary>
        /// összes kapcsolódó attribútumot tartalmazó tábla visszaadása
        /// </summary>
        /// <returns> összes kapcsolódó attribútumot tartalmazó tábla visszaadása </returns>
        private Dictionary<string, HashSet<string>> RelatedTables()
        {
            // először a külső kulcsokét
            Dictionary<string, HashSet<string>> tablesWithRelatedAttributes = TablesWithFKAttributes();

            // majd a nézet - tábla kapcsolatokét is (hozzáadva az előző listához)
            tablesWithRelatedAttributes = TablesWithViewAttributes(tablesWithRelatedAttributes);

            return tablesWithRelatedAttributes;
        }

        #endregion
    }
}
