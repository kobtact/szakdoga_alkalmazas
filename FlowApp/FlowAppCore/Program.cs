﻿using FlowAppCore.Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static FlowAppCore.Types.Datatypes;

namespace FlowAppCore
{
    class Program
    {
        static void Main(string[] args)
        {
            ConnectionController connCont = new ConnectionController("localhost", "3306", "root", "");
            DatabaseController controller = new DatabaseController();
            connCont.UpdateConnectionWithDatabase("sakila");
            controller.UpdateDatabase(connCont.GetConnection(), "sakila");

            /*Console.WriteLine("hoston elérhető adatbázisok nevei:");
            foreach (string database in connCont.GetAllDatabases())
            {
                Console.WriteLine(database);
            }*/

            List<Tuple<string, string>> viewDefs = controller.GetAllViewDefinition(connCont.GetConnection(), "sakila");
            Console.WriteLine(viewDefs.First().Item2 + " nézet definíciója: " + viewDefs.First().Item1);
            /*
            foreach(Tuple<string, string> def in viewDefs)
            {
                Console.WriteLine(def.Item1 + " : " + def.Item2);
            }
            */
            List<TableConnection> foreignConn = controller.GetForeignKeyRelations();
            /*Console.WriteLine("külső kulcsok a táblák közt:");
            foreach (TableConnection item in foreignConn)
            {
                Console.WriteLine(item.firstCol + " " + item.firstTable + " " + item.secondCol + " " + item.secondTable);
            }*/

            HashSet<TableConnection> relations = controller.GetViewRelations();
            /*Console.WriteLine("nézet - tábla kapcsolatok");
            foreach(TableConnection item in relations)
            {
                Console.WriteLine("'" + item.firstTable + "' nézet között kapcsolat van '" + item.secondTable + "' tábla '" + item.secondCol + "' oszlopával!");
            }*/

            Console.ReadKey();
        }
    }
}
