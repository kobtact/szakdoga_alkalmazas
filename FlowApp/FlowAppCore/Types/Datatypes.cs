﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlowAppCore.Types
{
    /*
     * Új adattípusokat itt definiálom
     */
    public class Datatypes
    {

        /// <summary>
        /// kapcsolódások (külső kulcs és nézet-tábla) az adateredet megjelenítéséhez <para/>
        /// <see cref="firstCol"/> : külső kulcs attribútum neve <para/>
        /// <see cref="firstTable"/> : külső kulcs tábla neve vagy nézet neve <para/>
        /// <see cref="secondCol"/> : referált attribútum neve <para/>
        /// <see cref="secondTable"/> : referált tábla neve
        /// </summary>
        public struct TableConnection
        {
            public string firstCol { get; set; }
            public string firstTable { get; set; }
            public string secondCol { get; set; }
            public string secondTable { get; set; }

            public override bool Equals(object obj)
            {
                if (!(obj is TableConnection))
                    return false;

                TableConnection other = (TableConnection)obj;

                if (firstCol == other.firstCol && firstTable == other.firstTable && secondCol == other.secondCol && secondTable == other.secondTable)
                    return true;
                else
                    return false;
            }

            public override int GetHashCode()
            {
                return firstCol.GetHashCode() ^ firstTable.GetHashCode() ^ secondCol.GetHashCode() ^ secondTable.GetHashCode();
            }

            public TableConnection(string firstCol, string firstTable, string secondCol, string secondTable)
            {
                this.firstCol = firstCol;
                this.firstTable = firstTable;
                this.secondCol = secondCol;
                this.secondTable = secondTable;
            }
        }
    }
}
