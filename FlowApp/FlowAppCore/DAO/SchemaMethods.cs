﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using static FlowAppCore.Types.Datatypes;

namespace FlowAppCore.DAO
{
    public class SchemaMethods
    {
        // lekérdezés sztring tárolására
        private string query;

        // nézetek nevei
        private List<string> viewNames;

        // táblák nevei
        private List<string> tableNames;

        // nézet definíciók
        private List<Tuple<string, string>> viewDefinitions;

        public SchemaMethods(MySqlConnection connection, string databaseName)
        {
            viewNames = GetAllViewNames(connection, databaseName);
            tableNames = GetAllTableNames(connection, databaseName);
            viewDefinitions = GetAllViewDefinition(connection, databaseName);
        }
        #region segédfüggvények
        /*
         *  visszaadja az összes nézet nevét 
         */
        public List<string> GetAllViewNames(MySqlConnection connection, string databaseName)
        {
            List<string> results = new List<string>();
            query = "SHOW FULL TABLES IN " + databaseName + " WHERE TABLE_TYPE LIKE 'VIEW'";
            using (MySqlCommand command = connection.CreateCommand())
            {
                command.CommandText = query;
                try
                {
                    using (MySqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            string view = reader.GetString("Tables_in_" + databaseName);
                            if (!string.IsNullOrEmpty(view))
                            {
                                results.Add(view);
                            }
                        }
                    }   
                }catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    return null;
                }
            }
            return results;
        }

        /*
         *  visszaadja az összes tábla (nézetek NEM) nevét
         */
        private List<string> GetAllTableNames(MySqlConnection connection, string databaseName)
        {
            List<string> results = new List<string>();
            query = "SHOW FULL TABLES IN " + databaseName + " WHERE TABLE_TYPE NOT LIKE 'VIEW'";
            using (MySqlCommand command = connection.CreateCommand())
            {
                command.CommandText = query;
                try
                {
                    using (MySqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            string view = reader.GetString("Tables_in_" + databaseName);
                            if (!string.IsNullOrEmpty(view))
                            {
                                results.Add(view);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    return null;
                }
            }
            return results;
        }
        /*
         *  A megfelelő nevű táblának visszaadja az oszlop neveit
         */
        private List<string> GetColumnNamesForTable(MySqlConnection connection, string databaseName, string tableName)
        {
            List<string> results = new List<string>();
            query = "DESCRIBE " + databaseName + "." + tableName;
            using(MySqlCommand command = connection.CreateCommand())
            {
                command.CommandText = query;
                try
                {
                    using (MySqlDataReader reader = command.ExecuteReader())
                    {
                        while(reader.Read())
                        {
                            string column = (string)reader["Field"];
                            if(!string.IsNullOrEmpty(column))
                            {
                                results.Add(column);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    return null;
                }
            }
            return results;
        }

        /*
         *   visszaadja az összes VIEW DDL definícióját stringként és a hozzá tartozó nézet nevét
         *   (hogy azt ne kelljen megint kikeresni az adatfolyam kapcsolat építésnél)
         */
        public List<Tuple<string, string>> GetAllViewDefinition(MySqlConnection connection, string databaseName)
        {
            List<Tuple<string, string>> results = new List<Tuple<string, string>>();
            MySqlCommand command;
            try
            {
                List<string> names = GetAllViewNames(connection, databaseName);
                // először kell az összes view neve
                foreach (string name in names)
                {
                    query = "SHOW CREATE VIEW " + name;
                    command = connection.CreateCommand();
                    command.CommandText = query;
                    using (MySqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            string definition = (string)reader["Create View"];
                            if (!string.IsNullOrEmpty(definition))
                            {
                                // UTF8 aposztróf kicserélése
                                definition = definition.Replace("\u0060", "'");
                                //definition = definition.Replace("'", "");
                                results.Add(new Tuple<string, string>(definition, name));
                            }
                        }
                    }
                }
            } catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
            return results;
        }
        #endregion
        /*
         *  information_schema metaadatok alapján
         *  szótár a táblák külső kulcsainak eltárolásához, a szótár kulcsa a külső kulcsot tartalmazó táblázat
         *  KAPCSOLÓDÓ TÁBLÁK MEGJELENÍTÉSÉHEZ KELL
         */
        public List<TableConnection> ForeignKeyConnections(MySqlConnection connection, string databaseName)
        {
            List<TableConnection> fkConn = new List<TableConnection>();

            query = "SELECT TABLE_NAME, COLUMN_NAME, REFERENCED_TABLE_NAME, REFERENCED_COLUMN_NAME FROM information_schema.KEY_COLUMN_USAGE " +
                "WHERE TABLE_NAME IS NOT NULL AND REFERENCED_TABLE_NAME IS NOT NULL AND CONSTRAINT_NAME NOT LIKE 'PRIMARY' AND TABLE_SCHEMA LIKE '" + databaseName + "'";
            using(MySqlCommand command = connection.CreateCommand())
            {
                command.CommandText = query;
                try
                {
                    using(MySqlDataReader reader = command.ExecuteReader())
                    {
                        while(reader.Read())
                        {
                            string foreignKey = reader.GetString("TABLE_NAME");
                            string foreignTable = reader.GetString("COLUMN_NAME");
                            string reference = reader.GetString("REFERENCED_TABLE_NAME");
                            string referenceTable = reader.GetString("REFERENCED_COLUMN_NAME");
                            if(!string.IsNullOrEmpty(reference) && !string.IsNullOrEmpty(referenceTable))
                            {
                                fkConn.Add(new TableConnection(foreignTable, foreignKey, referenceTable, reference));
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Source + " caused an error:" + ex.Message);
                    return null;
                }
            }
            return fkConn;
        }




        /*
         *     visszaadja a kapcsolódó oszlopokat a nézetek és táblák közt
         */
        public HashSet<TableConnection> TableRelations(MySqlConnection connection, string databaseName)
        {
            // kapcsolódó tábla-oszlop páros elemek listája
            HashSet<TableConnection> results = new HashSet<TableConnection>();

            // string a regexp kifejezéshez (táblák nevével kezdődő substringekről tudjuk, hogy van reláció)
            string regexTableExclude = string.Join("|", tableNames.ToArray()).TrimEnd('|');

            // alias táblanevekből eredő kapcsolódások
            HashSet<TableConnection> aliasConnections = new HashSet<TableConnection>();

            // megnézzük melyik táblák neve szerepel a definíciókban, aliasok alapján is
            // ha valamelyik szerepel, megnézzük, hogy mely oszlopnevek szerepelnek, ezek lesznek a kapcsolódóak
            foreach (Tuple<string, string> view in viewDefinitions)
            {
                foreach (string table in tableNames)
                {
                    if(view.Item1.Contains(table))
                    {
                        // kikeressük a táblához tartozó aliast (ha van)
                        string alias = GetAliasForTableInView(view.Item1, table);
                        List<string> columns = GetColumnNamesForTable(connection, databaseName, table);
                        foreach (string column in columns)
                        {
                            if (view.Item1.Contains("'" + table + "'.'" + column + "'")
                                || (!string.IsNullOrEmpty(alias) && view.Item1.Contains("'" + alias + "'.'" + column + "'")))
                            {
                                //Console.WriteLine(table + " " + column);
                                results.Add(new TableConnection("", view.Item2, column, table));
                            }
                        }
                    }
                }
            }
            return results;
        }

        /// <summary>
        /// megkeresi az aliast az adott nézet definícióban szereplő tábláhozs
        /// </summary>
        /// <param name="viewdef">a nézet definíció</param>
        /// <param name="table">a tábla</param>
        /// <returns>a tábla alias-ja vagy üres sztring</returns>
        private string GetAliasForTableInView(string viewdef, string table)
        {
            string alias = null;
            Match match = Regex.Match(viewdef, "(?i)('" + table + @"')\s'[a-z]+'");

            alias = match.ToString();

            if(!string.IsNullOrEmpty(alias))
            {
                alias = match.ToString().Split(null).ElementAt(1);

                // eltávolítjuk a maradék aposztrófokat is
                alias = Regex.Replace(alias, "'", "");
            }



            return alias;
        }
    }
}
