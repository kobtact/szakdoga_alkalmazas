﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlowAppCore.DAO
{
    public class ConnectionMethods
    {

        // lekérdezés sztring tárolására
        private string query;

        /// <summary>
        /// visszaadja az összes adatbázis nevét, ami a hoston található
        /// </summary>
        /// <param name="connection">aktuális MySQL csatlakozás objektum</param>
        /// <returns></returns>
        public List<string> GetAllDatabaseNames(MySqlConnection connection)
        {
            List<string> result = new List<string>();
            query = "SELECT SCHEMA_NAME AS 'Database' FROM information_schema.SCHEMATA WHERE SCHEMA_NAME NOT IN " +
                "('information_schema','mysql','performance_schema','phpmyadmin')";


            using (MySqlCommand command = connection.CreateCommand())
            {
                command.CommandText = query;
                using (MySqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        string databaseName = (string)reader["Database"];
                        result.Add(databaseName);
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// a paraméterben megadott adatbázisra állítja a kapcsolat objektumot
        /// </summary>
        /// <param name="databaseName">adatbázis neve</param>
        /// <param name="connection">aktuális MySQL csatlakozás objektum</param>
        /// <returns>sikeres volt-e</returns>
        public bool UpdateConnectionWithDatabase(MySqlConnection connection, string databaseName)
        {
            bool updateSucc = false;
            try
            {
                connection.ChangeDatabase(databaseName);
                updateSucc = true;
            }
            catch (MySqlException ex)
            {
                Console.WriteLine(ex);
            }
            return updateSucc;
        }
    }
}
